from urllib.request import Request, urlopen
import argparse
import os


def scrape_website(url):
    request = Request(url, headers={"User-Agent": "Mozilla/5.0"})
    return urlopen(request).read()


def save_page(page_name, page, save_dir="./data"):
    if not os.path.isdir(save_dir):
        os.makedirs(save_dir)

    with open("{}/{}.txt".format(save_dir, page_name), 'w') as f:
        f.write(page.decode("utf-8"))


def load_page(page_name, save_dir="./data"):
    with open("{}/{}.txt".format(save_dir, page_name), 'r') as f:
        lines = f.readlines()
    return lines


def save_original(url, page_name, save_dir="./data"):
    page = scrape_website(url)
    save_page(page_name + "_original", page, save_dir)


def compare_pages(page_name, save_dir="./data"):
    original_page = load_page(page_name + "_original", save_dir)
    current_page = load_page(page_name, save_dir)

    for i, line in enumerate(original_page):
        if line == current_page[i]:
            continue
        else:
            if "email&#160;protected" in line:
                continue
            else:
                return False
    return True

def send_alert(url):
    print("{} has been updated!".format(url))


def main(args):
    if not os.path.isdir(args.save_dir) or not os.path.isfile(
            "{}/{}_original.txt".format(args.save_dir, args.page_name)):
        save_original(args.url, args.page_name, args.save_dir)
    else:
        page = scrape_website(args.url)
        save_page(args.page_name, page, args.save_dir)

        if not compare_pages(args.page_name, args.save_dir):
            send_alert(args.url)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--url", help="url to check")
    parser.add_argument("--page_name", type=str, help="page filename")
    parser.add_argument("--save_dir",
                        type=str,
                        default="./data",
                        help="where to save files")
    args = parser.parse_args()

    main(args)
